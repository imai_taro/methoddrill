package method_drill;

public class Question03 {
	static void printRandomMessage() {
		int i = (int)(3 * Math.random());
		switch(i) {
		case 0 :
			System.out.println("こんばんは");
			break;
		case 1 :
			System.out.println("こんにちは");
			break;
		default :
			System.out.println("おはよう");
			break;
		}
	}
	public static void main(String[] args) {
		printRandomMessage();
		printRandomMessage();
		printRandomMessage();

	}

}
