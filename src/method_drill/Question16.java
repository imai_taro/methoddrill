package method_drill;

public class Question16 {
	static double getMinValue(double a, double b) {
		return Math.min(a, b);
	}
	public static void main(String[] args) {
		System.out.println(getMinValue(5.4, 2.8));
		System.out.println(getMinValue(10, 10.2));

	}

}
