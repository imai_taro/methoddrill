package method_drill;

public class Question15 {
	static boolean isEvenNumber(int value) {
		return (value % 2 == 0);

	}
	public static void main(String[] args) {
		System.out.println(isEvenNumber(2));
		System.out.println(isEvenNumber(5));
	}
}
