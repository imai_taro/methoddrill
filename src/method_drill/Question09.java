package method_drill;

public class Question09 {
	static void printMaxValue(double a, double b, double c) {
		double value[] = {a, b, c};
		double max = value[0];
		for(int i = 0; i < value.length; i++) {
			max = Math.max(max, value[i]);
		}
		System.out.println(max);
	}
	public static void main(String[] args) {
		printMaxValue(2, 7.5 ,4);
		printMaxValue(8, 1, 3);
		printMaxValue(1, 3, 9);

	}

}
