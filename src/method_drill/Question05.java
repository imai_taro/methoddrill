package method_drill;

public class Question05 {
	static void printCircleArea(double radius) {
		double circleArea = radius * radius * Math.PI;
		System.out.println(circleArea);
	}
	public static void main(String[] args) {
		printCircleArea(5);
		printCircleArea(2);

	}

}
