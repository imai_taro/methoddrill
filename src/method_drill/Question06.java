package method_drill;

public class Question06 {

	static void printRandomMessage(String name) {
		int i = (int)(3 * Math.random());
		switch(i) {
			case 0 :
				System.out.println("こんばんは" + name + "さん");
				break;
			case 1 :
				System.out.println("こんにちは" + name + "さん");
				break;
			default :
				System.out.println("おはよう" + name + "さん");
				break;
		}
	}


	public static void main(String[] args) {
		String name = "山田";
		printRandomMessage(name);
		printRandomMessage(name);
		printRandomMessage(name);
	}

}
