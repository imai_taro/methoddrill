package method_drill;

public class Question18 {
	static String getMessage(String name, boolean isKid) {
		if(isKid) {
			return "こんにちは。" + name + "ちゃん。";
		}else {
			return "こんにちは。" + name + "さん。";
		}
	}
	public static void main(String[] args) {
		System.out.println(getMessage("山田", true));
		System.out.println(getMessage("山田", false));
		System.out.println(getMessage("田中", false));

	}

}
