package method_drill;

public class Question20 {
	static double getAverage(double[] array) {
		double sum = 0;
		for(int i = 0; i < array.length; i++) {
			sum = sum + array[i];
		}
		double average = sum / array.length;
		return average;
	}
	public static void main(String[] args) {
		double[] value1 = {2,8,6,4};
		double[] value2= {1,3,5,7,2};
		System.out.println(getAverage(value1));
		System.out.println(getAverage(value2));

	}

}
