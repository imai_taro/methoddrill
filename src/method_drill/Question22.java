package method_drill;

public class Question22 {


	public static double getDistanceFromOrigin(Point p) {
		double distance = Math.sqrt(p.x * p.x + p.y * p.y);
		return distance;
	}

	public static void main(String[] args) {
		Point p = new Point(2, 3);
		System.out.println(getDistanceFromOrigin(p));

	}

}
