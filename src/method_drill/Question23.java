package method_drill;

public class Question23 {
	public static double getDistanceBetweenTwoPoints(Point p0, Point p1) {
		double distance = Math.sqrt((p0.x - p1.x)*(p0.x - p1.x) + (p0.y - p1.y)*(p0.y - p1.y));
		return distance;
	}

	public static void main(String[] args) {
		Point p0 = new Point(1,2);
		Point p1 = new Point(3,4);
		System.out.println(getDistanceBetweenTwoPoints(p0, p1));

	}

}
