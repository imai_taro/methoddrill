package method_drill;

public class Question21 {
	static String getLongestString(String[] array) {
		String longestStr = "";
		for(int i = 0; i < array.length; i++) {
			int length1 = array[i].length();
			int length2 = longestStr.length();
			if(length1 >= length2) {
				longestStr = array[i];
			}
		}
		return longestStr;
	}
	public static void main(String[] args) {
		String[] str1 = {"たまご","にたまご","あじつけたまご"};
		String[] str2 = {"にたまご","たまご","あじたま"};
		String[] str3 = {"たまご","あじつけたまご","にたまご"};
		System.out.println(getLongestString(str1));
		System.out.println(getLongestString(str2));
		System.out.println(getLongestString(str3));

	}

}
