package method_drill;

public class Question13 {
	static String getRandomMessage(String name) {
		int i = (int)(3 * Math.random());
		switch(i) {
		case 0 :
			return "こんばんは" + name + "さん";
		case 1 :
			return "こんにちは" + name + "さん";
		default :
			return "おはよう" + name + "さん";
		}
	}
	public static void main(String[] args) {
		System.out.println(getRandomMessage("山田"));
		System.out.println(getRandomMessage("田中"));
		System.out.println(getRandomMessage("佐藤"));

	}

}
