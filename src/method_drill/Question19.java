package method_drill;

public class Question19 {
	static int getMinValue(int[] array) {
		int min = array[0];
		for(int i = 0; i < array.length; i++) {
			min = Math.min(min, array[i]);
		}
		return min;
	}
	public static void main(String[] args) {
		int[] value1 = {1,2,3,4,5};
		int[] value2 = {8,5,9,4};
		System.out.println(getMinValue(value1));
		System.out.println(getMinValue(value2));

	}

}
