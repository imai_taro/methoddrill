package method_drill;

public class Question11 {
	static String getWeatherForecast() {
		String day = "";
		String weather = "";
		int i = (int)(3 * Math.random());
		int j = (int)(4 * Math.random());
		switch(i) {
		case 0 :
			day = "今日";
			break;
		case 1 :
			day = "明日";
			break;
		default :
			day = "明後日";
			break;
		}
		switch(j) {
		case 0 :
			weather = "晴れ";
			break;
		case 1 :
			weather = "曇り";
			break;
		case 2 :
			weather = "雨";
			break;
		default :
			weather = "雪";
			break;
		}
		String weatherForecast = day + "の天気は" + weather + "でしょう。";
		return weatherForecast;

	}
	public static void main(String[] args) {
		System.out.println(getWeatherForecast());
		System.out.println(getWeatherForecast());
		System.out.println(getWeatherForecast());
		System.out.println(getWeatherForecast());

	}

}
