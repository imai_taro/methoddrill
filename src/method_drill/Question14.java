package method_drill;

public class Question14 {
	static double getAbsoluteValue(double value) {
		return Math.abs(value);
	}
	public static void main(String[] args) {
		System.out.println(getAbsoluteValue(2.5));
		System.out.println(getAbsoluteValue(-10.8));

	}

}
